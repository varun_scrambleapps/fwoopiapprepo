package com.fortyau.fwoopi.service;

import com.fortyau.fwoopi.mmssms.Message;
import com.fortyau.fwoopi.transaction.NotificationManager;
import com.fortyau.fwoopi.transaction.SmsHelper;
import com.fortyau.fwoopi.ui.popup.QKReplyActivity;
import com.fortyau.fwoopi.mmssms.Transaction;
import com.fortyau.fwoopi.data.ConversationLegacy;
import com.pushbullet.android.extension.MessagingExtension;

public class PushbulletService extends MessagingExtension {
    private final String TAG = "PushbulletService";

    @Override
    protected void onMessageReceived(String conversationIden, String body) {
        long threadId = Long.parseLong(conversationIden);
        ConversationLegacy conversation = new ConversationLegacy(getApplicationContext(), threadId);

        Transaction sendTransaction = new Transaction(getApplicationContext(), SmsHelper.getSendSettings(getApplicationContext()));
        Message message = new Message(body, conversation.getAddress());
        message.setType(Message.TYPE_SMSMMS);
        sendTransaction.sendNewMessage(message, conversation.getThreadId());

        QKReplyActivity.dismiss(conversation.getThreadId());

        NotificationManager.update(getApplicationContext());
    }

    @Override
    protected void onConversationDismissed(String conversationIden) {
        long threadId = Long.parseLong(conversationIden);
        ConversationLegacy conversation = new ConversationLegacy(getApplicationContext(), threadId);
        conversation.markRead();
    }

}
