package com.fortyau.fwoopi.ui.messagelist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter {
    public static final String KEY_ID = "id";
    public static final String KEY_AD = "ad";
    public static final String KEY_TIME = "time";

    private static final String TAG = "DBAdapter";
    private static final String DATABASE_NAME = "db_ad";
    private static final String DATABASE_TABLE = "ad_master";
    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_CREATE = "create table ad_master (id integer primary key autoincrement, "
            + " ad text, time text);";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(DATABASE_CREATE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS ad_master");
            onCreate(db);
        }
    }

    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        DBHelper.close();
    }

    public void insertAd(String ad, String time) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_AD, ad);
        initialValues.put(KEY_TIME, time);
        int i = db.update(DATABASE_TABLE, initialValues, KEY_AD + " = ?",
                new String[]{ad});
        Log.e("UPDATE", "X: " + i);
        if (i <= 0) {
            long l = db.insert(DATABASE_TABLE, null, initialValues);
            Log.e("INSERT", "X: " + l);
        }
    }

    public boolean deleteAd(String ad) {
        boolean a = db.delete(DATABASE_TABLE, KEY_AD + "=?", new String[]{ad}) > 0;
        Log.e("DELETE", "X: " + a);
        return a;
    }

    public Cursor getAd(String ad) throws SQLException {
        Cursor mCursor = db.query(true, DATABASE_TABLE, new String[]{
                        KEY_ID, KEY_TIME}, KEY_AD + "=?", new String[]{ad}, null, null,
                null, null);
        String query = "SELECT * FROM ad_master WHERE ad=" + ad;
//        Cursor mCursor = db.rawQuery(query, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }
}

