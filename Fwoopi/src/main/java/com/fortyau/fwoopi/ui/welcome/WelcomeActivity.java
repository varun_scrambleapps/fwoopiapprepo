package com.fortyau.fwoopi.ui.welcome;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.fortyau.fwoopi.R;
//import com.fortyau.fwoopi.api.ApiInterface;
//import com.fortyau.fwoopi.control.UserController;
//import com.fortyau.fwoopi.datamodel.UserAccess;
import com.fortyau.fwoopi.ui.MainActivity;
import com.fortyau.fwoopi.ui.base.QKActivity;
//import com.fortyau.fwoopi.ui.tutorial.Demo1;


public class WelcomeActivity extends AppCompatActivity implements Runnable {

    public static final int WELCOME_REQUEST_CODE = 31415;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

//        getSupportActionBar().hide();

        Thread t = new Thread(this);
        t.start();
    }


    public void startWelcome() {
//        UserAccess ua = UserAccess.getInstance(getApplicationContext());
//        if (ua.isFirst()) {
//            Intent i = new Intent(this, Demo1.class);
//            startActivity(i);
//            this.finish();
//        } else {
//            if (UserController.getInstance().getUser() == null) {
//                if (ua.getId() != null)
//                    UserController.getInstance().setUser(ApiInterface.getUserInfoFromNetwork(ua.getId()));
//                else {
//                    Intent i = new Intent(this, LoginActivity.class);
//                    startActivity(i);
//                    this.finish();
//                }
//            }

            // if (UserController.getInstance().isLoggedIn()){
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            this.finish();

//        }
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                wait(3000);
            }
        } catch (InterruptedException e) {
//            e.printStackTrace();
        } finally {
            startWelcome();
        }
    }

}
