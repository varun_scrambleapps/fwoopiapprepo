package com.fortyau.fwoopi.old.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.fortyau.fwoopi.R;

import java.util.HashMap;
import java.util.Map.Entry;


public class AppPreference {
    private static SharedPreferences sharedPreferences = null;

    public static void openPref(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name) + "Ad",
                Context.MODE_PRIVATE);
    }

    public static void deleteKey(Context context, String key) {
        HashMap<String, String> result = new HashMap<String, String>();

        AppPreference.openPref(context);
        for (Entry<String, ?> entry : AppPreference.sharedPreferences.getAll()
                .entrySet()) {
            result.put(entry.getKey(), (String) entry.getValue());
        }

        boolean b = result.containsKey(key);
        if (b) {
            AppPreference.openPref(context);
            Editor prefsPrivateEditor = AppPreference.sharedPreferences.edit();
            prefsPrivateEditor.remove(key);

            prefsPrivateEditor.commit();
            prefsPrivateEditor = null;
            AppPreference.sharedPreferences = null;
        }
    }

    public static void setDetail(Context context, String key, String value) {
        AppPreference.openPref(context);
        Editor prefsPrivateEditor = AppPreference.sharedPreferences.edit();
        prefsPrivateEditor.putString(key, value);

        prefsPrivateEditor.commit();
        prefsPrivateEditor = null;
        AppPreference.sharedPreferences = null;
    }

    public static Boolean checkDetail(Context context, String key) {
        HashMap<String, String> result = new HashMap<String, String>();

        AppPreference.openPref(context);
        for (Entry<String, ?> entry : AppPreference.sharedPreferences.getAll()
                .entrySet()) {
            result.put(entry.getKey(), (String) entry.getValue());
        }

        boolean b = result.containsKey(key);
        return b;
    }

    public static String getDetail(Context context, String key) {
        HashMap<String, String> result = new HashMap<String, String>();

        AppPreference.openPref(context);
        for (Entry<String, ?> entry : AppPreference.sharedPreferences.getAll()
                .entrySet()) {
            result.put(entry.getKey(), (String) entry.getValue());
        }

        String b = result.get(key);
        return b;

    }
}
