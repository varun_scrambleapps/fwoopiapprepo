package com.fortyau.fwoopi.old.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.ui.tutorial.Demo1;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;


public class SplashScreenActivity extends ActionBarActivity implements Runnable {

    SharedPrefsManager sharedPrefsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        sharedPrefsManager = new SharedPrefsManager(this);
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {

        try {
            synchronized (this) {
                wait(3000);
            }
        } catch (InterruptedException ignored) {
        } finally {
//            if (sharedPrefsManager.retreiveBoolean(Constants.IS_FIRST_TIME)) {
//                Intent i = new Intent(this, Demo1.class);
//                startActivity(i);
//                this.finish();
//            } else {
                if (sharedPrefsManager.retreiveBoolean(Constants.IS_LOGIN)) {
                    UserController.getInstance().setUser(ApiInterface.getUserInfoFromLocale(SplashScreenActivity.this));
                    if (UserController.getInstance().getUser() == null) {
                        Intent i = new Intent(this, LoginActivity.class);
                        startActivity(i);
                        this.finish();
                    } else {
                        startActivity(new Intent(this, com.fortyau.fwoopi.ui.MainActivity.class));
                        finish();
                    }

                } else {
                    startActivity(new Intent(this, Demo1.class));
                    finish();
                }
//            }

        }

    }
}
