package com.fortyau.fwoopi.old.datamodel;

import java.util.ArrayList;

public class UserStatistics {

	private String clicksSinceLastPayout;
	private String earnedSinceLastPayout;
	private String lastPayoutRequestTime;
	private String lastPayoutAmount;
	private String lastPayoutTime;
	private String totalEarnedYTD;
	private String totalEarned;
	private ArrayList<Category> categoriesWithEarnings;


	/**
	 * @return the clicksSinceLastPayout
	 */
	public String getClicksSinceLastPayout() {
		return clicksSinceLastPayout;
	}

	/**
	 * @param clicksSinceLastPayout
	 *            the clicksSinceLastPayout to set
	 */
	public void setClicksSinceLastPayout(String clicksSinceLastPayout) {
		this.clicksSinceLastPayout = clicksSinceLastPayout;
	}

	/**
	 * @return the earnedSinceLastPayout
	 */
	public String getEarnedSinceLastPayout() {
		return earnedSinceLastPayout;
	}

	/**
	 * @param earnedSinceLastPayout
	 *            the earnedSinceLastPayout to set
	 */
	public void setEarnedSinceLastPayout(String earnedSinceLastPayout) {
		this.earnedSinceLastPayout = earnedSinceLastPayout;
	}

	/**
	 * @return the lastPayoutRequestTime
	 */
	public String getLastPayoutRequestTime() {
		return lastPayoutRequestTime;
	}

	/**
	 * @param lastPayoutRequestTime
	 *            the lastPayoutRequestTime to set
	 */
	public void setLastPayoutRequestTime(String lastPayoutRequestTime) {
		this.lastPayoutRequestTime = lastPayoutRequestTime;
	}

	/**
	 * @return the lastPayoutAmount
	 */
	public String getLastPayoutAmount() {
		return lastPayoutAmount;
	}

	/**
	 * @param lastPayoutAmount
	 *            the lastPayoutAmount to set
	 */
	public void setLastPayoutAmount(String lastPayoutAmount) {
		this.lastPayoutAmount = lastPayoutAmount;
	}

	/**
	 * @return the lastPayoutTime
	 */
	public String getLastPayoutTime() {
		return lastPayoutTime;
	}

	/**
	 * @param lastPayoutTime
	 *            the lastPayoutTime to set
	 */
	public void setLastPayoutTime(String lastPayoutTime) {
		this.lastPayoutTime = lastPayoutTime;
	}

	/**
	 * @return the totalEarnedYTD
	 */
	public String getTotalEarnedYTD() {
		return totalEarnedYTD;
	}

	/**
	 * @param totalEarnedYTD
	 *            the totalEarnedYTD to set
	 */
	public void setTotalEarnedYTD(String totalEarnedYTD) {
		this.totalEarnedYTD = totalEarnedYTD;
	}

	/**
	 * @return the totalEarned
	 */
	public String getTotalEarned() {
		return totalEarned;
	}

	/**
	 * @param totalEarned
	 *            the totalEarned to set
	 */
	public void setTotalEarned(String totalEarned) {
		this.totalEarned = totalEarned;
	}

	/**
	 * @return the categoriesWithEarnings
	 */
	public ArrayList<Category> getCategoriesWithEarnings() {
		return categoriesWithEarnings;
	}

	/**
	 * @param categoriesWithEarnings
	 *            the categoriesWithEarnings to set
	 */
	public void setCategoriesWithEarnings(ArrayList<Category> categoriesWithEarnings) {
		this.categoriesWithEarnings = categoriesWithEarnings;
	}

}
