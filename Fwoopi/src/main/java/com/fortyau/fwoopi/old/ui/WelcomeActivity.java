package com.fortyau.fwoopi.old.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.ui.tutorial.Demo1;
import com.fortyau.fwoopi.old.utils.AppPreference;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.ui.MainActivity;
import com.fortyau.fwoopi.ui.view.QKTextView;


public class WelcomeActivity extends FwoopiActivity implements Runnable {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);

        Thread t = new Thread(this);
        t.start();

        if (!AppPreference.checkDetail(getApplicationContext(), "AD_TITLE")) {
            AppPreference.setDetail(getApplicationContext(), "AD_TITLE", Constants.DEFAULT_FWOOPI_LONG_MSG + "\n" + Constants.DEFAULT_FWOOPI_SHORT_MSG);
        }
    }


    public void startWelcome() {
        UserAccess ua = UserAccess.getInstance(getApplicationContext());
        if (ua.isFirst()) {
            Intent i = new Intent(this, Demo1.class);
            startActivity(i);
            this.finish();
        } else {
            if (UserController.getInstance().getUser() == null) {
                if (ua.getId() != null)
                    UserController.getInstance().setUser(ApiInterface.getUserInfoFromNetwork(ua.getId()));
                else {
                    Intent i = new Intent(this, LoginActivity.class);
                    startActivity(i);
                    this.finish();
                }
            }

            // if (UserController.getInstance().isLoggedIn()){
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
            this.finish();

        }
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                wait(3000);
            }
        } catch (InterruptedException e) {
//            e.printStackTrace();
        } finally {
            startWelcome();
        }
    }

    @Override
    public void handleBackArrow(View view) {

    }

    @Override
    public void handleNextArrow(View view) {

    }
}
