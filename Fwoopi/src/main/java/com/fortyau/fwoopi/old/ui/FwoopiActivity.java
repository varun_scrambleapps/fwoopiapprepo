package com.fortyau.fwoopi.old.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;

public abstract class FwoopiActivity extends Activity{

	abstract public void handleBackArrow(View view);

	abstract public void handleNextArrow(View view);
}