package com.fortyau.fwoopi.old.ui.tutorial;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.ui.FwoopiActivity;

public class Demo2 extends FwoopiActivity implements View.OnClickListener {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_2);

        Button next = (Button) findViewById(R.id.button1);
        next.setText("Next");
        next.setTextColor(Color.WHITE);
        next.setOnClickListener(this);

        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText(Html.fromHtml(getString(R.string.demoBeginNote)));

        tv = (TextView) findViewById(R.id.textView3);
        tv.setText(Html.fromHtml(getString(R.string.howItWorks)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button1:
                Intent i = new Intent(this, Demo3.class);
                startActivityForResult(i,1);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void handleBackArrow(View view) {

    }

    @Override
    public void handleNextArrow(View view) {

    }
}
