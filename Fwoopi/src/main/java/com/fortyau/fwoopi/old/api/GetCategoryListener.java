package com.fortyau.fwoopi.old.api;

import java.util.List;

/**
 * Created by susan on 1/16/15.
 */
public interface GetCategoryListener<T> {
    public void requestGetCategoryStarted();
    public void requestGetCategoryFinished(List<T> modelList);
}
