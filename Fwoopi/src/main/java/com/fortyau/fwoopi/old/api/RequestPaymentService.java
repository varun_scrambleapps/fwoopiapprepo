package com.fortyau.fwoopi.old.api;

import android.os.AsyncTask;

import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by susan on 1/16/15.
 */
public class RequestPaymentService extends AsyncTask<Void, Void, Void> {
    private boolean b;
    private RequestPaymentServiceListener listener;

    public RequestPaymentService(RequestPaymentServiceListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.requestPaymentTaskStarted();
    }

    @Override
    protected Void doInBackground(Void... params) {
        String id = UserController.getInstance().getUser().getId();
        String password = UserAccess.getInstance().getPassword();
        String requestPaymentUrl = (Constants.activeUrl + Constants.API_URL_USER_PAYMENT).replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), id);
        Utils.showDebugLog(this.getClass()+":Request Payment Url: "+requestPaymentUrl);
        try {
            ArrayList<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair(Constants.API_PARAM_POST_KEY, Constants.apiKey));
            nvps.add(new BasicNameValuePair(Constants.API_USER_PASSWORD, password));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nvps);
            URL url = new URL(requestPaymentUrl);
            HttpsURLConnection request = (HttpsURLConnection) url.openConnection();
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            request.setRequestMethod("POST");
            OutputStream post = request.getOutputStream();
            entity.writeTo(post);
            post.flush();
            if (request.getResponseCode() == 200) {
                b = true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        b =  false;
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.requestPaymentTaskFinished(b);
    }
}
