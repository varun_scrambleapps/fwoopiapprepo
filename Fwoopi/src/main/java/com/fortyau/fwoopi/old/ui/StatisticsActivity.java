package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.RequestPaymentService;
import com.fortyau.fwoopi.old.api.RequestPaymentServiceListener;
import com.fortyau.fwoopi.old.api.StatisticsService;
import com.fortyau.fwoopi.old.api.StatisticsServiceListener;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.datamodel.UserStatistics;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;
import com.fortyau.fwoopi.old.utils.Utils;

import java.text.DecimalFormat;


public class StatisticsActivity extends FwoopiActivity implements StatisticsServiceListener, RequestPaymentServiceListener {

    private boolean waitingForLogin;// = false;
    private TextView t1;
    private TextView t2;
    private TextView t3;
    private TextView t4;
    private TextView t5;
    private UserStatistics us;
    private ProgressDialog progressDialog;
    SharedPrefsManager sharedPrefsManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats);
        sharedPrefsManager = new SharedPrefsManager(this);
        if (!sharedPrefsManager.retreiveBoolean(Constants.IS_LOGIN)) {
            waitingForLogin = true;
            Intent i = new Intent(this, SignInActivity.class);
            startActivity(i);
        }
        progressDialog = new ProgressDialog(this);
        t1 = (TextView) findViewById(R.id.textView1);
        t2 = (TextView) findViewById(R.id.textView2);
        t3 = (TextView) findViewById(R.id.textView3);
        t4 = (TextView) findViewById(R.id.textView4);
        t5 = (TextView) findViewById(R.id.textView5);
        t5.setText(Constants.MSG_PAYOUT_THRESHOLD);
        Button next = (Button) findViewById(R.id.next);
        next.setText("Request Payout");
        next.setTextColor(Color.WHITE);
        String id = UserController.getInstance().getUser().getId();
        if (id == null || "".equals(id))
            id = UserAccess.getInstance(getApplicationContext()).getId();

        if (id == null || "".equals(id)) {
            return;
        }
        new StatisticsService(this).execute(id);
        t1.setTextColor(Color.BLACK);
        t2.setTextColor(Color.BLACK);
        t3.setTextColor(Color.DKGRAY);
        t4.setTextColor(Color.DKGRAY);
    }

    protected void onResume() {
        super.onResume();
        if (!UserController.getInstance().isLoggedIn() && waitingForLogin) {
            waitingForLogin = false;
            this.finish();
        }
    }

    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub
    }

    @Override
    public void handleNextArrow(View view) {
        if (!Utils.isNetworkAvailable((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
            Toast.makeText(this, "No network connection, please continue once you have internet access", Toast.LENGTH_SHORT)
                    .show();
        } else {
            new RequestPaymentService(this).execute();
        }
    }

    @Override
    public void requestUserStatisticsStarted(){
        progressDialog.setMessage(Constants.MSG_PROCESSING);
        progressDialog.show();
    }

    @Override
    public void requestUserStatisticsFinished(UserStatistics userStats) {
        progressDialog.dismiss();
        us = userStats;
        if (us.getClicksSinceLastPayout() == null || us.getClicksSinceLastPayout().equals("0")) {
            t3.setText("0");
        } else {
            t3.setText(us.getClicksSinceLastPayout());
        }
        DecimalFormat df = new DecimalFormat("0.00");
        double dollarz = .05 * Integer.parseInt(us.getClicksSinceLastPayout());
        if (us.getTotalEarned() == null || us.getTotalEarned().equals("0")) {
            t4.setText("$0.00");
        } else {
            t4.setText("$" + df.format(dollarz));
        }
    }

    @Override
    public void requestPaymentTaskStarted() {
        progressDialog.setMessage(Constants.MSG_PROCESSING);
        progressDialog.show();
    }

    @Override
    public void requestPaymentTaskFinished(boolean b) {
        progressDialog.dismiss();
        if (b) {
            Toast.makeText(this, "Payout Request is now processing!", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, Constants.MSG_PAYOUT_THRESHOLD, Toast.LENGTH_LONG).show();
            t5.setText(Constants.MSG_PAYOUT_THRESHOLD);
        }
    }
}
