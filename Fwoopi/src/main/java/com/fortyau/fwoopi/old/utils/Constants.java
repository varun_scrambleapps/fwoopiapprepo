package com.fortyau.fwoopi.old.utils;

import android.view.Menu;

/**
 * Created by chandramohanjayaswal on 4/16/16.
 */
public class Constants {
    //region Private Utility Methods

    //endregion

    //region Public Utility Methods

    //endregion

    //region Action Methods

    //endregion

    //region Object Methods

    //endregion

    //region Delegate Methods

    //endregion

    /*******RESEARCH************
        1) http://andro-source.blogspot.com
        2) http://stackoverflow.com/questions/3012287/how-to-read-mms-data-in-android
        3) http://forum.xda-developers.com/showthread.php?t=2222703
        4) http://www.codeproject.com/Articles/1044639/Android-SMS-MMS-API-Sending-SMS-MMS-Receiving-SMS
        5) http://codetheory.in/android-sms/
        6) http://stackoverflow.com/questions/3811416/sending-mms-programatically-on-android
    *******RESEARCH************/

    public static final String LOG_TAG                          = "FWOOPI";
    public static final int BACK_TO_PREVIOUS_CODE               = 1;
    public static final String KEY_MESSAGES                     = "message_parcel";
    public static final String KEY_USER_ID                      = "thread_id_extra";
    public static final String KEY_PICTURE                      = "picture_extra";
    public static final String KEY_SENDERS                      = "SENDERS";
    public static final String NAME_EXTRA                       = "name_extra";
    public static final String KEY_SEND_COUNT                   = "send_count_extra";
    public static final int ADD_AFTER_COUNT                     = 3;
    public static final int DEFAULT_TIMEOUT_MS                  = 50000;
    public static final String DEFAULT_FWOOPI_LONG_MSG          = "Fwoopi! Make money while you text!";
    public static final String DEFAULT_FWOOPI_SHORT_MSG         = "www.f-w.co/start";
    public static final String ADD_MSG_THREAD_MARKER            = "Fwoopi___";



    public static final long TYPE_SENT                          = 2;
    public static final long TYPE_RECEIVED                      = 1;
    public static final int STATUS_SENT                         = -1;
    public static final int STATUS_NOT_SENT                     = 0;
    public static final int STATUS_DELIVERED                    = 99;
    public static final int STATUS_NOT_DELIVERED                = -99;
    public static final int INTENT_CODE_LOAD_IMAGE              = 10;

    // //////////////////////////////////////
    // MESSAGE ACTIVITY CONTEXT MENU SETTINGS
    // //////////////////////////////////////

    // SMS
    public static final int MSG_CONTEXT_MENU_CALL_ID            = 100;
    public static final int MSG_CONTEXT_MENU_CALL_ORDER         = Menu.FIRST;
    public static final String MSG_CONTEXT_MENU_CALL_TITLE      = "Call";

    public static final int MSG_CONTEXT_MENU_FORWARD_ID         = 101;
    public static final int MSG_CONTEXT_MENU_FORWARD_ORDER      = Menu.FIRST + 1;
    public static final String MSG_CONTEXT_MENU_FORWARD_TITLE   = "Forward";

    public static final int MSG_CONTEXT_MENU_COPY_ID            = 102;
    public static final int MSG_CONTEXT_MENU_COPY_ORDER         = Menu.FIRST + 2;
    public static final String MSG_CONTEXT_MENU_COPY_TITLE      = "Copy Text";

    public static final int MSG_CONTEXT_MENU_DELETE_ID          = 103;
    public static final int MSG_CONTEXT_MENU_DELETE_ORDER       = Menu.FIRST + 3;
    public static final String MSG_CONTEXT_MENU_DELETE_TITLE    = "Delete";

    public static final int MSG_CONTEXT_MENU_PICTURE_ID         = 104;

    // MMS
    public static final int MSG_CONTEXT_MENU_SAVE_ID            = 105;
    public static final int MSG_CONTEXT_MENU_SAVE_ORDER         = Menu.FIRST;
    public static final String MSG_CONTEXT_MENU_SAVE_TITLE      = "Save";

    public static final int MSG_CONTEXT_MENU_VIEW_ID            = 106;
    public static final int MSG_CONTEXT_MENU_VIEW_ORDER         = Menu.FIRST + 1;
    public static final String MSG_CONTEXT_MENU_VIEW_TITLE      = "View";

    public static final int MSG_CONTEXT_MENU_MMS_DELETE_ID      = 107;
    public static final int MSG_CONTEXT_MENU_MMS_DELETE_ORDER   = Menu.FIRST + 2;
    public static final String MSG_CONTEXT_MENU_MMS_DELETE_TITLE = "Delete";
    public static final String MSG_PAYOUT_THRESHOLD             = "You cannot request a payout for earnings of less than $10.00";
    public static final String MSG_PROCESSING                   = "Processing...";


    // API HARD CODES
    public static final String API_USER_ID_REPLACEMENT          = "[ID]";

    private static boolean test                                 = false;
    private static String demoUrl                               = "https://demo.tenfastfeet.com/fwoopi/api/";
    private static String liveUrl                               = "https://fwoopi.com/api/";
    public static final String activeUrl                        = test ? demoUrl : liveUrl;

    private static final String API_URL_USER_CREATE             = "user/create";
    private static final String API_URL_USER_UPDATE             = "user/update/" + Constants.API_USER_ID_REPLACEMENT;
    private static final String API_URL_USER_LOGIN              = "user/login";

    public static final String apiKey                           = "b41d43fc58b0fc1f047f0c5307a95cdd0e8402e5";
    public static final String IS_LOGIN                         = "is_login";
    public static final String PREFERENCE_NAME                  = "fwoopiPrefs";
    public static final String USER_JSON                        = "user_json";
    public static final String NO_NETWORK                       = "Internet Not Available";
    public static final String ADD_COUNT                        = "add_count";
    public static final String IS_FIRST_TIME                    = "is_first_time";

    public static final String API_URL_CATEGORIES               = "category?";
    public static final String API_URL_USER_DATA                = "user/" + Constants.API_USER_ID_REPLACEMENT + "?";
    public static final String API_URL_USER_STATS               = "user/statistics/" + Constants.API_USER_ID_REPLACEMENT + "?";
    public static final String API_URL_USER_AD                  = "user/next-ad/" + Constants.API_USER_ID_REPLACEMENT + "?";
    public static final String API_URL_USER_CANCEL              = "user/cancel/" + Constants.API_USER_ID_REPLACEMENT;
    public static final String API_URL_USER_PAYMENT             = "user/request-payment/" + Constants.API_USER_ID_REPLACEMENT;
    public static final String API_URL_USER_VERIFY              = "user/verify/" + Constants.API_USER_ID_REPLACEMENT;

    public static final String API_PARAM_KEY                    = "api_key=";
    public static final String API_PARAM_POST_KEY               = "api_key";

    public static final String URL_UPDATE_USER                  = activeUrl + API_URL_USER_UPDATE;
    public static final String CATEGORIES_URL                   = activeUrl + API_URL_CATEGORIES + API_PARAM_KEY + apiKey;
    public static final String API_GET_CATEGORIES               = activeUrl + API_URL_CATEGORIES + API_PARAM_KEY
            + apiKey;
    public static final String URL_LOGIN                        = activeUrl + API_URL_USER_LOGIN;
    public static final String URL_CREATE_USER                  = activeUrl + API_URL_USER_CREATE;

    public static final String API_GET_USER                     = "API_GET_USER";
    public static final String API_GET_USER_STATS               = activeUrl + API_URL_USER_STATS + API_PARAM_KEY
            + apiKey;
    public static final String API_GET_AD                       = "API_GET_AD";
    public static final String API_CREATE_USER                  = "API_CREATE_USER";
    public static final String API_UPDATE_USER                  = "API_UPDATE_USER";
    public static final String API_CANCEL_USER                  = "API_CANCEL_USER";
    public static final String API_REQUEST_PAYMENT              = "API_REQUEST_PAYMENT";
    public static final String API_VERIFY                       = "API_VERIFY";
    public static final String API_CATEGORY_ID                  = "Id";
    public static final String API_CATEGORY_NAME                = "Classification";

    public static final String API_USER_FIRSTNAME               = "FirstName";
    public static final String API_USER_LASTNAME                = "LastName";
    public static final String API_USER_ID_FIELD                = "Id";
    public static final String API_USER_EMAIL                   = "Email";
    public static final String API_USER_PHONE                   = "Phone";
    public static final String API_USER_ADDRESS                 = "Address";
    public static final String API_USER_CITY                    = "City";
    public static final String API_USER_STATE                   = "State";
    public static final String API_USER_ZIP                     = "Zip";
    public static final String API_USER_CATEGORIES              = "Categories";
    public static final String API_USER_CATEGORIES_ARRAY        = "Categories[]";
    public static final String API_USER_CATEGORY1               = "Category1";
    public static final String API_USER_CATEGORY2               = "Category2";
    public static final String API_USER_PASSWORD                = "Password";
    public static final String API_USER_NEW_PASSWORD            = "newPassword";

    public static final String API_AD_ID                        = "Id";
    public static final String API_AD_TEXT                      = "Text";
    public static final String API_AD_LAST                      = "LastClickTime";

    public static final String API_USER_STAT_CLICKS             = "ClicksSinceLastPayout";
    public static final String API_USER_STAT_EARNED             = "EarnedSinceLastPayout";
    public static final String API_USER_STAT_LAST_PAY_REQUEST   = "LastPayoutRequestTime";
    public static final String API_USER_STAT_LAST_PAY_AMOUNT    = "LastPayoutAmount";
    public static final String API_USER_STAT_LAST_PAY_TIME      = "LastPayoutTime";
    public static final String API_USER_STAT_TOTAL_EARNED       = "TotalEarned";
    public static final String API_USER_STAT_TOTAL_YTD          = "TotalEarnedYTD";
    public static final String API_USER_STAT_BY_CATS            = "EarningsByCategory";
    public static final String API_USER_STAT_CAT                = "Category";
    public static final String API_USER_CANCEL_CONFIRM          = "Confirm";
    public static final String API_USER_CANCEL_DECLINE_PAYMENT  = "DeclinePayment";

    public final static String PREFS_FILE 			            = "userprefs";
    public final static String FIRST 				            = "firstUse";
    public final static String PASSWORD 			            = "fwoopiPassword";
    public final static String ID 					            = "fwoopiId";
}
