package com.fortyau.fwoopi.old.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

/*
 * Straight up copy / patched version of Google's RecipientIdCache
 * 
 */
public class FwoopiIdCache {
	private static final boolean LOCAL_DEBUG = false;
	private static final String TAG = "Mms/cache";

	private static Uri sAllCanonical = Uri.parse("content://mms-sms/canonical-addresses");

	private static Uri sSingleCanonicalAddressUri = Uri.parse("content://mms-sms/canonical-address");

	private static FwoopiIdCache sInstance;

	static FwoopiIdCache getInstance() {
		return sInstance;
	}

	private final Map<Long, String> mCache;

	private final Context mContext;

	public static class Entry {
		public long id;
		public String number;

		public Entry(long id, String number) {
			this.id = id;
			this.number = number;
		}
	};

	static void init(Context context) {
		sInstance = new FwoopiIdCache(context);
		new Thread(new Runnable() {
			public void run() {
				fill();
			}
		}).start();
	}

	FwoopiIdCache(Context context) {
		mCache = new HashMap<Long, String>();
		mContext = context;
	}

	public static void fill() {
		Context context = sInstance.mContext;
		Cursor c = context.getContentResolver().query(sAllCanonical, null, null, null, null);
		if (c == null) {
			Log.w(TAG, "null Cursor in fill()");
			return;
		}

		try {
			synchronized (sInstance) {
				// Technically we don't have to clear this because the stupid
				// canonical_addresses table is never GC'ed.
				sInstance.mCache.clear();
				while (c.moveToNext()) {
					// TODO: don't hardcode the column indices
					long id = c.getLong(0);
					String number = c.getString(1);
					sInstance.mCache.put(id, number);
				}
			}
		} finally {
			c.close();
		}
	}

	public static List<Entry> getAddresses(String spaceSepIds) {
		synchronized (sInstance) {
			List<Entry> numbers = new ArrayList<Entry>();
			String[] ids = spaceSepIds.split(" ");
			for (String id : ids) {
				long longId;

				try {
					longId = Long.parseLong(id);
				} catch (NumberFormatException ex) {
					// skip this id
					continue;
				}

				String number = sInstance.mCache.get(longId);

				if (number == null) {
					Log.w(TAG, "RecipientId " + longId + " not in cache!");


					fill();
					number = sInstance.mCache.get(longId);
				}

				if (TextUtils.isEmpty(number)) {
					Log.w(TAG, "RecipientId " + longId + " has empty number!");
				} else {
					numbers.add(new Entry(longId, number));
				}
			}
			return numbers;
		}
	}



	public static void canonicalTableDump() {
		Log.d(TAG, "**** Dump of canoncial_addresses table ****");
		Context context = sInstance.mContext;
		Cursor c = context.getContentResolver().query(sAllCanonical, null, null, null, null);
		if (c == null) {
			Log.w(TAG, "null Cursor in content://mms-sms/canonical-addresses");
		}
		try {
			while (c.moveToNext()) {
				// TODO: don't hardcode the column indices
				long id = c.getLong(0);
				String number = c.getString(1);
				Log.d(TAG, "id: " + id + " number: " + number);
			}
		} finally {
			c.close();
		}
	}

	/**
	 * getSingleNumberFromCanonicalAddresses looks up the recipientId in the
	 * canonical_addresses table and returns the associated number or email
	 * address.
	 * 
	 * @param context
	 *            needed for the ContentResolver
	 * @param recipientId
	 *            of the contact to look up
	 * @return phone number or email address of the recipientId
	 */
	public static String getSingleAddressFromCanonicalAddressInDb(final Context context, final String recipientId) {
		Cursor c = context.getContentResolver().query(ContentUris.withAppendedId(sSingleCanonicalAddressUri, Long.parseLong(recipientId)), null, null, null, null);

		if (c == null) {
			Log.w(TAG, "null Cursor looking up recipient: " + recipientId);
			return null;
		}
		try {
			if (c.moveToFirst()) {
				String number = c.getString(0);
				return number;
			}
		} finally {
			c.close();
		}
		return null;
	}

	// used for unit tests
	public static void insertCanonicalAddressInDb(final Context context, String number) {

		Log.d(TAG, "[RecipientIdCache] insertCanonicalAddressInDb: number=" + number);


		final ContentValues values = new ContentValues();
		values.put("address", number);

		final ContentResolver cr = context.getContentResolver();

		// We're running on the UI thread so just fire & forget, hope for the
		// best.
		// (We were ignoring the return value anyway...)
		new Thread("updateCanonicalAddressInDb") {
			public void run() {
				cr.insert(sAllCanonical, values);
			}
		}.start();
	}

}