package com.fortyau.fwoopi.old.api;

/**
 * Created by susan on 1/16/15.
 */
public interface RequestPaymentServiceListener {
    public void requestPaymentTaskStarted();
    public void requestPaymentTaskFinished(boolean b);
}
