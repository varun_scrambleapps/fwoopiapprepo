package com.fortyau.fwoopi.old.datamodel;

/**
 * Created by macbook on 3/14/16.
 */
public class SMS {
    String sender;
    String body;

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
