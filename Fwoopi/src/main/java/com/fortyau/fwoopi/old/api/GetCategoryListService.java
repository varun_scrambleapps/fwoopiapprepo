package com.fortyau.fwoopi.old.api;

import android.os.AsyncTask;
import android.util.Log;

import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.datamodel.Category;
import com.fortyau.fwoopi.old.api.GetCategoryListener;
import com.fortyau.fwoopi.old.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by susan on 1/16/15.
 */
public class GetCategoryListService extends AsyncTask<Void, Void, Void> {
    private List<Category> categoryList;
    private GetCategoryListener<Category> listener;

    public GetCategoryListService(GetCategoryListener<Category> listener) {
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(Void... params) {
        categoryList = getAdCategories();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.requestGetCategoryFinished(categoryList);
    }

    private List<Category> getAdCategories() {
        List<Category> returnList = new ArrayList<>();
        try {
            URL categoriesUrl = new URL(Constants.API_GET_CATEGORIES);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) categoriesUrl.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.connect();
            int statusCode = httpsURLConnection.getResponseCode();
            if (statusCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();
                String json = sb.toString();
                if ("".equals(json.trim())) {
                    return null;
                }
                JSONArray parsedJson = new JSONArray(json);
                JSONObject catEntry;
                for (int i = 0; i < parsedJson.length(); i++) {
                    catEntry = parsedJson.getJSONObject(i);
                    Category cat = new Category();
                    cat.setId(catEntry.getString(Constants.API_CATEGORY_ID));
                    cat.setName(catEntry.getString(Constants.API_CATEGORY_NAME));
                    returnList.add(cat);
                }
                return returnList;
            }

        } catch (IOException | JSONException ioException) {
            ioException.printStackTrace();
        }

        return null;
    }
}
