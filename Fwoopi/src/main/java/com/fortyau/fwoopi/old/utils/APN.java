package com.fortyau.fwoopi.old.utils;

/**
 * Created by chandramohanjayaswal on 4/19/16.
 */
public class APN {
    public String MMSCenterUrl = "";
    public String MMSPort = "";
    public String MMSProxy = "";

    public APN(String MMSCenterUrl, String MMSPort, String MMSProxy)
    {
        this.MMSCenterUrl = MMSCenterUrl;
        this.MMSPort = MMSPort;
        this.MMSProxy = MMSProxy;
    }

    public APN()
    {

    }
}