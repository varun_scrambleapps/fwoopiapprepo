package com.fortyau.fwoopi.old.control;

import java.util.*;

import com.fortyau.fwoopi.old.datamodel.*;

public class UserController {

	private static UserController instance;
	private User currentUser;
	private boolean loggedIn;
	private ArrayList<Map<String, String>> mPeopleList;

	private UserController() {

	}

	public static UserController getInstance() {
		if (instance == null)
			instance = new UserController();
		return instance;
	}

	public User getUser() {
		return this.currentUser;
	}

	public void setUser(User u) {
		this.currentUser = u;
	}

	/**
	 * @return the loggedIn
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}

	/**
	 * @param loggedIn
	 *            the loggedIn to set
	 */
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	/**
	 * @param mPeopleList
	 *            the mPeopleList to set
	 */
	public void setContactList(ArrayList<Map<String, String>> mPeopleList) {
		this.mPeopleList = mPeopleList;
	}

	/**
	 * @return the mPeopleList
	 */
	public ArrayList<Map<String, String>> getContactList() {
		return mPeopleList;
	}
}