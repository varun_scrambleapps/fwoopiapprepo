package com.fortyau.fwoopi.old.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.control.*;
import com.fortyau.fwoopi.old.datamodel.*;

public class Setup1 extends FwoopiActivity {

    public static Activity fwoopiActivity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fwoopiActivity = this;
        setContentView(R.layout.setup_one);
        Button next = (Button) findViewById(R.id.next);
        next.setText("Continue");
        next.setTextColor(Color.WHITE);
        TextView tv = (TextView) findViewById(R.id.infoAlert);
        tv.setText(Html.fromHtml(getString(R.string.payoutText)));
    }

    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub
    }

    @Override
    public void handleNextArrow(View view) {
        EditText nameEdit = (EditText) findViewById(R.id.editText1);
        EditText emailEdit = (EditText) findViewById(R.id.EditText01);
        EditText addressEdit = (EditText) findViewById(R.id.EditText02);
        EditText cityEdit = (EditText) findViewById(R.id.EditText03);
        EditText zipEdit = (EditText) findViewById(R.id.EditText05);
        Spinner stateSpinner = (Spinner) findViewById(R.id.Spinner1);

        String name = nameEdit.getText().toString();
        if (name.length() <= 0 || name.split(" ") == null || name.split(" ").length != 2) {
            // User needs to enter first/last
            Toast.makeText(this, "Please enter your first and last name, like First Last", Toast.LENGTH_SHORT).show();
            return;
        }
        String firstName = name.split(" ")[0];
        String lastName = name.split(" ")[1];

        String email = emailEdit.getText().toString();
        if (email.length() <= 0 || !email.contains("@")) {
            Toast.makeText(this, "Please enter your email address", Toast.LENGTH_SHORT).show();
            return;
        }

        String address = addressEdit.getText().toString();
        if (address.length() <= 0) {
            Toast.makeText(this, "Please enter your address", Toast.LENGTH_SHORT).show();
            return;
        }

        String city = cityEdit.getText().toString();
        if (city.length() <= 0) {
            Toast.makeText(this, "Please enter your city", Toast.LENGTH_SHORT).show();
            return;
        }

        String zip = zipEdit.getText().toString();
        if (zip.length() <= 0) {
            Toast.makeText(this, "Please enter your zip", Toast.LENGTH_SHORT).show();
            return;
        }

        String state = (String) stateSpinner.getSelectedItem();
        if (state.length() <= 0) {
            Toast.makeText(this, "Please enter your state", Toast.LENGTH_SHORT).show();
            return;
        }

        User u = new User();

        u.setFirstName(firstName);
        u.setLastName(lastName);
        u.setEmail(email);
        u.setAddress(address);
        u.setCity(city);
        u.setZip(zip);
        u.setState(state);

        UserController.getInstance().setUser(u);

        Intent i = new Intent(this, Setup2.class);
        i.putExtra("user", u);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
