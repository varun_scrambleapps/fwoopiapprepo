package com.fortyau.fwoopi.old.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.utils.Constants;

public class Setup2 extends FwoopiActivity {

    User user;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_two);

        user = getIntent().getParcelableExtra("user");

        Button next = (Button) findViewById(R.id.button1);
        next.setText("Continue");
        next.setTextColor(Color.WHITE);

        TextView tv = (TextView) findViewById(R.id.infoAlert);
        tv.setText(Html.fromHtml(getString(R.string.passwordNote)));
    }

    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleNextArrow(View view) {

        EditText passwordEdit = (EditText) findViewById(R.id.password);
        EditText confirmEdit = (EditText) findViewById(R.id.confirmPassword);

        String password = passwordEdit.getText().toString();
        String confirm = confirmEdit.getText().toString();

        if (password.length() <= 0) {
            Toast.makeText(this, "Please enter a password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (confirm.length() <= 0) {
            Toast.makeText(this, "Please confirm your password", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!password.equals(confirm)) {
            Toast.makeText(this, "Your passwords do not match", Toast.LENGTH_SHORT).show();
            return;
        }
        user.setPassword(password);

        Intent i = new Intent(this, Setup3.class);
        i.putExtra("user", user);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
