package com.fortyau.fwoopi.old.api;

import android.os.AsyncTask;

import com.fortyau.fwoopi.old.datamodel.Category;
import com.fortyau.fwoopi.old.datamodel.UserStatistics;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by susan on 1/16/15.
 */
public class StatisticsService extends AsyncTask<String, Void, Void> {
    private StatisticsServiceListener listner;
    private UserStatistics userStats;

    public StatisticsService(StatisticsServiceListener listner) {
        this.listner = listner;
    }

    @Override
    protected void onPreExecute() {
        listner.requestUserStatisticsStarted();
    }

    @Override
    protected Void doInBackground(String... params) {
        userStats = getUserStats(params[0]);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listner.requestUserStatisticsFinished(userStats);
    }

    private UserStatistics getUserStats(String id) {
        UserStatistics returnUserStats = new UserStatistics();
        try {
            URL userStatURL = new URL(Constants.API_GET_USER_STATS.replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), id));
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) userStatURL.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.connect();
            int statusCode = httpsURLConnection.getResponseCode();
            if (statusCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();
                String json = sb.toString();
                if ("".equals(json.trim())) {
                    return null;
                }
                Utils.showDebugLog(this.getClass()+":User Statistics Response: "+json);
                JSONObject parsedJson = new JSONObject(json);
                returnUserStats.setClicksSinceLastPayout(parsedJson.getString(Constants.API_USER_STAT_CLICKS));
                returnUserStats.setEarnedSinceLastPayout(parsedJson.getString(Constants.API_USER_STAT_EARNED));
                returnUserStats.setLastPayoutAmount(parsedJson.getString(Constants.API_USER_STAT_LAST_PAY_AMOUNT));
                returnUserStats.setLastPayoutRequestTime(parsedJson.getString(Constants.API_USER_STAT_LAST_PAY_REQUEST));
                returnUserStats.setLastPayoutTime(parsedJson.getString(Constants.API_USER_STAT_LAST_PAY_TIME));
                returnUserStats.setTotalEarned(parsedJson.getString(Constants.API_USER_STAT_TOTAL_EARNED));
                returnUserStats.setTotalEarnedYTD(parsedJson.getString(Constants.API_USER_STAT_TOTAL_YTD));
                JSONObject cats = parsedJson.getJSONObject(Constants.API_USER_STAT_BY_CATS);
                ArrayList<Category> userCategories = new ArrayList<>();
                String s;

                @SuppressWarnings("unchecked")
                Iterator<String> i = cats.keys();

                while (i.hasNext()) {
                    s = i.next();
                    Category category = new Category();
                    category.setName(s);
                    category.setAmount(cats.getString(s));
                    userCategories.add(category);
                }
                returnUserStats.setCategoriesWithEarnings(userCategories);
                return returnUserStats;
            }
        } catch (IOException | JSONException ioException) {
            ioException.printStackTrace();
        }

        return null;
    }
}
