package com.fortyau.fwoopi.old.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateUtils;
import android.util.Log;

import com.fortyau.fwoopi.old.datamodel.Ad;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Chronelab Technology on 3/27/2016.
 */
public class Utils {

    public static boolean isReturnedFromMessageApp = false;

    public static void reset() {
        isReturnedFromMessageApp = false;
    }

    public static void showDebugLog(String log) {
        Log.i(Constants.LOG_TAG, log);
    }


    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isNetworkAvailable(ConnectivityManager cm) {
        NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static boolean compareNumbers(String num1, String num2) {

        if (num1 == null || num2 == null)
            return false;

        num1 = num1.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(" ", "").replaceAll("\\-", "").replaceAll("\\+", "");
        num2 = num2.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(" ", "").replaceAll("\\-", "").replaceAll("\\+", "");

        if (num1.length() != num2.length()) {
            if (num1.length() > num2.length())
                num1 = num1.substring(num1.length() - num2.length());
            else
                num2 = num2.substring(num2.length() - num1.length());

        }
        return num1.equals(num2);

    }

    public static String stripNumber(String number) {
        return number.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll(" ", "").replaceAll("\\-", "").replaceAll("\\+", "");
    }

    public static String appendDefaultFwoopiAddInMessage(String texSMSMessage) {
        texSMSMessage += texSMSMessage.length() < 125 ? "\n" + Constants.DEFAULT_FWOOPI_LONG_MSG : "";
        texSMSMessage += texSMSMessage.length() < 140 ? "\n" + Constants.DEFAULT_FWOOPI_SHORT_MSG : "";
        return texSMSMessage;
    }

    public static String parseAddResponse(String response, String texSMSMessage) {
        Utils.showDebugLog("utils.Utils: ParseAddResponse " + response);
        try {
            Ad returnAd = new Ad();
            JSONObject parsedJson = new JSONObject(response);
            returnAd.setId(parsedJson.getString(Constants.API_AD_ID));
            returnAd.setText(parsedJson.getString(Constants.API_AD_TEXT));
            returnAd.setLastSent(parsedJson.getLong(Constants.API_AD_LAST));
            if (returnAd.getText() != null) {
                if (texSMSMessage.length() + returnAd.getText().length() < 160)
                    texSMSMessage += texSMSMessage.length() < 120 ? " \n\n" + returnAd.getText() : "";
            } else {
                texSMSMessage = Utils.appendDefaultFwoopiAddInMessage(texSMSMessage);
            }
        } catch (JSONException e) {
            texSMSMessage = Utils.appendDefaultFwoopiAddInMessage(texSMSMessage);
        } finally {

        }
        return texSMSMessage;
    }

    public static List<String> extractUrls(String text) {
        ArrayList links = new ArrayList();

        String regex = "\\(?\\b(www[.])[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while (m.find()) {
            String urlStr = m.group();
            if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
                urlStr = urlStr.substring(1, urlStr.length() - 1);
            }
            links.add(urlStr);
        }
        return links;
    }

    public static String getCurrentDateTime() {
        Date date = new Date();
        SimpleDateFormat dateFormatWithZone = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormatWithZone.format(date);
    }

    public static String getTimeInMillisecond() {
        return String.valueOf(Calendar.getInstance().getTimeInMillis());
    }

    public static boolean checkAdClickEligibility(Context context, String id) {
        long startMillis = Long.parseLong(AppPreference.getDetail(context, id));
//        Calendar startTime = Calendar.getInstance();
//        startTime.setTimeInMillis(millis);

        long now = System.currentTimeMillis();
        long difference = now - startMillis;

        long differenceInSeconds = difference / DateUtils.SECOND_IN_MILLIS;

        if (differenceInSeconds / 60 >= 30)
            return true;
        else
            return false;
    }
}
