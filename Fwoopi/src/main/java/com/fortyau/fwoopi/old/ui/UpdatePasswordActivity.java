package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.control.*;
import com.fortyau.fwoopi.old.datamodel.*;

public class UpdatePasswordActivity extends FwoopiActivity implements Runnable {

	private User user;
	private ProgressDialog pd;
	private Handler handler = new Handler() {

		public void handleMessage(Message msg) {
			if (user == null) {
				pd.dismiss();
				if (ErrorController.getInstance().hasError()) {
					Toast.makeText(UpdatePasswordActivity.this, "Sorry!  We failed to update your password. "
							+ ErrorController.getInstance().getErrorMessage(), Toast.LENGTH_LONG).show();
				}else {
					Toast.makeText(UpdatePasswordActivity.this, "Sorry!  We failed to update your password.", Toast.LENGTH_LONG).show();

				}
				return;
			}


			EditText passwordEdit = (EditText) findViewById(R.id.password);
			EditText confirmEdit = (EditText) findViewById(R.id.newPassword);
			EditText confirmNewEdit = (EditText) findViewById(R.id.confirmNewPassword);

			passwordEdit.setText("");
			confirmEdit.setText("");
			confirmNewEdit.setText("");

			pd.dismiss();

			Toast.makeText(getApplicationContext(), "Your password was successfully updated!", Toast.LENGTH_LONG).show();
		}
	};

	public void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);

		setContentView(R.layout.update_password);

		Button next = (Button) findViewById(R.id.button1);
		next.setText("Continue");
		next.setTextColor(Color.WHITE);


	}

	@Override
	public void handleBackArrow(View view) {



	}

	@Override
	public void handleNextArrow(View view) {
		User u = new User();

		EditText passwordEdit = (EditText) findViewById(R.id.password);
		EditText confirmEdit = (EditText) findViewById(R.id.newPassword);
		EditText confirmNewEdit = (EditText) findViewById(R.id.confirmNewPassword);

		String password = passwordEdit.getText().toString();
		String confirm = confirmEdit.getText().toString();
		String newConfirm = confirmNewEdit.getText().toString();

		if (password.length() <= 0) {
			Toast.makeText(this, "Please enter your old password", Toast.LENGTH_SHORT).show();
			return;
		}

		if (confirm.length() <= 0) {
			Toast.makeText(this, "Please enter your new password", Toast.LENGTH_SHORT).show();
			return;
		}

		if (newConfirm.length() <= 0) {
			Toast.makeText(this, "Please confirm your new password", Toast.LENGTH_SHORT).show();
			return;
		}

		if (!newConfirm.equals(confirm)) {
			Toast.makeText(this, "Your new passwords do not match", Toast.LENGTH_SHORT).show();
			return;
		}

		u.setPassword(password);
		u.setNewPassword(confirm);

		user = u;
		pd = ProgressDialog.show(this, "Changing Password", "We are changing your password, one moment please..");
		Thread t = new Thread(this);
		t.start();
	}

	@Override
	public void run() {
		user = ApiInterface.updateUserPassword(user);
		handler.sendEmptyMessage(0);

	}

}
