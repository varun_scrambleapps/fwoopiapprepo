package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.api.GetCategoryListService;
import com.fortyau.fwoopi.old.api.GetCategoryListener;
import com.fortyau.fwoopi.old.control.ErrorController;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.Category;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;
import com.fortyau.fwoopi.old.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends FwoopiActivity implements Runnable, GetCategoryListener<Category> {

    private ProgressDialog pd;
    private boolean waitingForLogin;
    private int whichToCall;
    private List<Category> categories;
    private EditText nameEdit;
    private Spinner catSpinner;
    private EditText emailEdit;
    private EditText cityEdit;
    private EditText addressEdit;
    private EditText zipEdit;
    private Spinner stateSpinner;
    private User user;
    SharedPrefsManager sharedPrefsManager;

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (user == null) {
                pd.dismiss();
                if (ErrorController.getInstance().hasError()) {
                    Toast.makeText(SettingsActivity.this, "Sorry!  We failed to update your account. "
                            + ErrorController.getInstance().getErrorMessage(), Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(SettingsActivity.this, "Sorry!  We failed to update your account.", Toast.LENGTH_LONG).show();
                }
            } else {
                pd.dismiss();
                Toast.makeText(SettingsActivity.this, "Your settings have been updated", Toast.LENGTH_LONG).show();
                UserController.getInstance().setUser(user);

            }
        }
    };


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        sharedPrefsManager = new SharedPrefsManager(this);
        if (!sharedPrefsManager.retreiveBoolean(Constants.IS_LOGIN)) {
            waitingForLogin = true;
            Intent i = new Intent(this, SignInActivity.class);
            startActivity(i);
            this.finish();
        }


        nameEdit = (EditText) findViewById(R.id.editText1);
        emailEdit = (EditText) findViewById(R.id.EditText01);
        addressEdit = (EditText) findViewById(R.id.EditText02);
        cityEdit = (EditText) findViewById(R.id.EditText03);
        zipEdit = (EditText) findViewById(R.id.EditText05);
        stateSpinner = (Spinner) findViewById(R.id.Spinner1);
        catSpinner = (Spinner) findViewById(R.id.Spinner2);
        pd = new ProgressDialog(this);
        Button next = (Button) findViewById(R.id.next);
        next.setText("Update");
        next.setTextColor(Color.WHITE);
        next = (Button) findViewById(R.id.updatePass);
        next.setText("Update Password");
        next.setTextColor(Color.WHITE);
        TextView tv = (TextView) findViewById(R.id.infoAlert);
        tv.setText(Html.fromHtml(getString(R.string.categoryNote)));

        if (!Utils.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
            Toast.makeText(this, "No network connection, please continue once you have internet access", Toast.LENGTH_LONG).show();
            return;
        }

        user = UserController.getInstance().getUser();

        if (user == null) {
            user = ApiInterface.getUserInfoFromNetwork(UserAccess.getInstance(getApplicationContext()).getId());
        }

        if (user == null) {
            Toast.makeText(this, "Unable to retrieve your information from the Fwoopi Servers!  Please try again later.", Toast.LENGTH_LONG).show();
            this.finish();
            return;
        }

        nameEdit.setText(user.getFirstName() + " " + user.getLastName());
        emailEdit.setText(user.getEmail());
        addressEdit.setText(user.getAddress());
        cityEdit.setText(user.getCity());
        zipEdit.setText(user.getZip());

        Resources res = getResources();
        String[] array = res.getStringArray(R.array.stateList);
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(user.getState())) {
                stateSpinner.setSelection(i);
                break;
            }
        }
//        whichToCall = 0;
//        Thread t = new Thread(this);
//        t.start();

        new GetCategoryListService(this).execute();
    }

    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleNextArrow(View view) {
        // TODO Auto-generated method stub

    }

    public void updatePassword(View view) {
        Intent i = new Intent(this, UpdatePasswordActivity.class);
        startActivityForResult(i, 0);
    }


    protected void onResume() {
        super.onResume();
        if (!UserController.getInstance().isLoggedIn() && waitingForLogin) {
            waitingForLogin = false;
            this.finish();
        }
    }


    public void updateUser(View view) {
        String name = nameEdit.getText().toString();
        if (name.length() <= 0 || name.split(" ") == null || name.split(" ").length != 2) {
            // User needs to enter first/last
            Toast.makeText(this, "Please enter your first and last name, like First Last", Toast.LENGTH_SHORT).show();
            return;
        }
        String firstName = name.split(" ")[0];
        String lastName = name.split(" ")[1];

        String email = emailEdit.getText().toString();
        if (email.length() <= 0 || !email.contains("@")) {
            Toast.makeText(this, "Please enter your email address", Toast.LENGTH_SHORT).show();
            return;
        }

        String address = addressEdit.getText().toString();
        if (address.length() <= 0) {
            Toast.makeText(this, "Please enter your address", Toast.LENGTH_SHORT).show();
            return;
        }

        String city = cityEdit.getText().toString();
        if (city.length() <= 0) {
            Toast.makeText(this, "Please enter your city", Toast.LENGTH_SHORT).show();
            return;
        }

        String zip = zipEdit.getText().toString();
        if (zip.length() <= 0) {
            Toast.makeText(this, "Please enter your zip", Toast.LENGTH_SHORT).show();
            return;
        }

        String state = (String) stateSpinner.getSelectedItem();
        if (state.length() <= 0) {
            Toast.makeText(this, "Please enter your state", Toast.LENGTH_SHORT).show();
            return;
        }

        User u = new User();

        u.setFirstName(firstName);
        u.setLastName(lastName);
        u.setEmail(email);
        u.setAddress(address);
        u.setCity(city);
        u.setZip(zip);
        u.setState(state);

        Category cat = (Category) catSpinner.getSelectedItem();
        ArrayList<Category> catList = new ArrayList<Category>();
        catList.add(cat);
        u.setCategories(catList);
        user = u;
        pd = ProgressDialog.show(this, "Updating Account", "We are updating your information, one moment please..");
        Thread t = new Thread(this);
        t.start();

    }

    public void run() {
        user = ApiInterface.updateUser(user);
        handler.sendEmptyMessage(0);
    }

    @Override
    public void requestGetCategoryStarted() {
        pd.setMessage("Getting Categories.");
        pd.show();
    }

    @Override
    public void requestGetCategoryFinished(List<Category> modelList) {
        categories = modelList;
        Log.i("Cat array size", categories.size() + "");
        if (categories == null || categories.size() == 0) {
            Toast.makeText(this, "Unable to retrieve information from the Fwoopi Servers!  Please try again later. at line 136", Toast.LENGTH_LONG).show();
            this.finish();
            return;
        }
        ArrayAdapter<Category> cats = new ArrayAdapter<Category>(this, android.R.layout.simple_spinner_item, categories);
        catSpinner.setAdapter(cats);
        if (user.getCategories().size() != 0) {
            for (int i = 0; i < categories.size(); i++) {
                if (categories.get(i).getId().equals(user.getCategories().get(0).getId())) {
                    catSpinner.setSelection(i);
                    break;
                }
            }
        }
        pd.dismiss();
    }
}
