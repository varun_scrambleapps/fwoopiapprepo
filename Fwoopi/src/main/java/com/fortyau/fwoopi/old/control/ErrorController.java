package com.fortyau.fwoopi.old.control;

public class ErrorController {

	private String errorMessage;
	private boolean hasError;

	private static ErrorController instance;

	private ErrorController() {

	}

	public static ErrorController getInstance() {
		if (instance == null)
			instance = new ErrorController();
		return instance;
	}


	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean hasError() {
		return this.hasError;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}



}
