package com.fortyau.fwoopi.old.datamodel;

public class Ad {

	private String id;
	private String text;
	private long lastSent;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the lastSent
	 */
	public long getLastSent() {
		return lastSent;
	}

	/**
	 * @param lastSent
	 *            the lastSent to set
	 */
	public void setLastSent(long lastSent) {
		this.lastSent = lastSent;
	}

}
