package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.Category;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;
import com.fortyau.fwoopi.old.utils.Utils;
import com.fortyau.fwoopi.ui.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Setup4 extends FwoopiActivity {

    private User user;
    private ProgressDialog pd;
    private StringRequest request;
    private DefaultRetryPolicy defaultRetryPolicy;
    private RequestQueue requestQueue;
    SharedPrefsManager sharedPrefsManager;
    User returnUser;
    String error;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_four);

        Button next = (Button) findViewById(R.id.button1);
        next.setText("Let's Go!");
        next.setTextColor(Color.WHITE);
        user = getIntent().getParcelableExtra("user");
        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText(Html.fromHtml(getString(R.string.doneNote)));
        sharedPrefsManager = new SharedPrefsManager(this);
        requestQueue = Volley.newRequestQueue(this);
        defaultRetryPolicy = new DefaultRetryPolicy(5000, 1, 1);

        request = new StringRequest(Request.Method.POST, Constants.URL_CREATE_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utils.showDebugLog(this.getClass()+" :"+response);
                pd.dismiss();
                JSONObject parsedJson = null;
                try {
                    parsedJson = new JSONObject(response);
                    sharedPrefsManager.saveString(Constants.USER_JSON, response);
                    sharedPrefsManager.saveBoolean(Constants.IS_LOGIN, true);
                    sharedPrefsManager.saveBoolean(Constants.IS_FIRST_TIME,true);
                    String id = parsedJson.getString(Constants.API_USER_ID_FIELD);
                    if (id != null && !id.equalsIgnoreCase("")) {
                        User user = ApiInterface.parseUserJsonResponseData(response);
                        if (user != null){
                            UserController.getInstance().setUser(user);
                            UserAccess.getInstance(getApplicationContext()).setId(user.getId());
                            if(!TextUtils.isEmpty(user.getPassword())){
                                UserAccess.getInstance(getApplicationContext()).setPassword(user.getPassword());
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setResult(RESULT_OK);
                finish();

//                Intent intent = new Intent(Setup4.this, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                finish();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(Setup4.this, "Problem:: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.API_PARAM_POST_KEY, Constants.apiKey);
                params.put(Constants.API_USER_FIRSTNAME, user.getFirstName());
                params.put(Constants.API_USER_LASTNAME, user.getLastName());
                params.put(Constants.API_USER_ADDRESS, user.getAddress());
                params.put(Constants.API_USER_CITY, user.getCity());
                params.put(Constants.API_USER_STATE, user.getState());
                params.put(Constants.API_USER_ZIP, user.getZip());
                params.put(Constants.API_USER_EMAIL, user.getEmail());
                String phone;
                if(user.getPhone()==null){
                    phone = "6155551234";
                }else {
                    phone = user.getPhone();
                }
                params.put(Constants.API_USER_PHONE, phone);
                params.put(Constants.API_USER_PASSWORD, user.getPassword());
                int i=0;
                for(Category category: user.getCategories()){
                    params.put(Constants.API_USER_CATEGORIES+"["+i+"]", "1");
                    i++;
                }
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError){
                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }
                error = volleyError.getMessage();
                Log.i("etMessage ","error:: "+error);
                if(TextUtils.isEmpty(error)){
                    error = "Somthing went wrong!!!";
                }else{
                    error = error.substring(3);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Setup4.this, "Problem:: " + error, Toast.LENGTH_LONG).show();
                    }
                });

                pd.dismiss();

                return volleyError;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String credentials = "AppText.HTACCESSUNAME" + ":" + "AppText.HTACCESSPASSWORD";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", "Basic " + base64EncodedCredentials);
                params.put("Content type", "form-data");
                return params;
            }
        };

    }



    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleNextArrow(View view) {
        if(Utils.isNetworkAvailable(this)){
            pd = ProgressDialog.show(this, "Registering", "We are registering your account, please wait.");
            requestQueue.add(request);
        }else{
            Toast.makeText(this,Constants.NO_NETWORK, Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Constants.BACK_TO_PREVIOUS_CODE)
            this.finish();
    }

    public void onBackPressed() {
        this.setResult(Constants.BACK_TO_PREVIOUS_CODE);
        this.finish();
    }
}
