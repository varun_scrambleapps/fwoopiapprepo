package com.fortyau.fwoopi.old.api;

import com.fortyau.fwoopi.old.datamodel.UserStatistics;

/**
 * Created by susan on 1/16/15.
 */
public interface StatisticsServiceListener {
    public void requestUserStatisticsStarted();
    public void requestUserStatisticsFinished(UserStatistics userStats);

}
