package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;
import com.fortyau.fwoopi.old.utils.Utils;
import com.fortyau.fwoopi.ui.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends FwoopiActivity {

    private ProgressDialog pd;
    private StringRequest request;
    private DefaultRetryPolicy defaultRetryPolicy;
    private RequestQueue requestQueue;
    SharedPrefsManager sharedPrefsManager;
    private String username, password;
    private EditText usernameEdit, passwordEdit;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Utils.showDebugLog(this.getClass()+" : onCreate");
        usernameEdit = (EditText) findViewById(R.id.username);
        passwordEdit = (EditText) findViewById(R.id.password);
        Button next = (Button) findViewById(R.id.button1);
        next.setText("Login");
        next.setTextColor(Color.WHITE);
        sharedPrefsManager = new SharedPrefsManager(this);
        requestQueue = Volley.newRequestQueue(this);
        defaultRetryPolicy = new DefaultRetryPolicy(5000, 1, 1);

        request = new StringRequest(Request.Method.POST, Constants.URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utils.showDebugLog(this.getClass()+" :"+response);
                pd.dismiss();
                JSONObject parsedJson = null;
                try {
                    parsedJson = new JSONObject(response);
                    sharedPrefsManager.saveString(Constants.USER_JSON, response);
                    sharedPrefsManager.saveBoolean(Constants.IS_LOGIN, true);
                    String id = parsedJson.getString(Constants.API_USER_ID_FIELD);
                    if (id != null && !id.equalsIgnoreCase("")) {
                        User user = ApiInterface.parseUserJsonResponseData(response);
                        user.setPassword(password);
                        if (user != null){
                            Utils.showDebugLog("Id: "+user.getId()+" Password: "+user.getPassword());
                            UserController.getInstance().setUser(user);
                            UserAccess.getInstance(getApplicationContext()).setId(user.getId());
                            if(!TextUtils.isEmpty(user.getPassword())){
                                UserAccess.getInstance(getApplicationContext()).setPassword(user.getPassword());
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Toast.makeText(LoginActivity.this, "Problem:: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.API_PARAM_POST_KEY, Constants.apiKey);
                params.put(Constants.API_USER_EMAIL, username);
                params.put(Constants.API_USER_PASSWORD, password);
                return params;
            }

            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError){
                if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
                    VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
                    volleyError = error;
                }
                final String error = volleyError.getMessage();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(LoginActivity.this, "Problem:: " + error.substring(3), Toast.LENGTH_LONG).show();
                    }
                });
                pd.dismiss();
                return volleyError;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String credentials = "AppText.HTACCESSUNAME" + ":" + "AppText.HTACCESSPASSWORD";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                params.put("Authorization", "Basic " + base64EncodedCredentials);
                params.put("Content type", "form-data");
                return params;
            }
        };
    }


    public void handleNextArrow(View view) {
        username = usernameEdit.getText().toString();
        password = passwordEdit.getText().toString();
        if (username.length() <= 0 || !username.contains("@")) {
            Toast.makeText(this, "Please enter an email address", Toast.LENGTH_SHORT).show();
            return;
        }
        if (password.length() <= 0) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(Utils.isNetworkAvailable(this)){
            request.setRetryPolicy(defaultRetryPolicy);
            pd = ProgressDialog.show(this, "Registering", "We are registering your account, please wait.");
            requestQueue.add(request);
        }else {
            Toast.makeText(this, Constants.NO_NETWORK, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub

    }
}
