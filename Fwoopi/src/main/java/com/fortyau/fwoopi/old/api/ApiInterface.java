package com.fortyau.fwoopi.old.api;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.fortyau.fwoopi.old.control.ErrorController;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.Ad;
import com.fortyau.fwoopi.old.datamodel.Category;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.datamodel.UserStatistics;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;
import com.fortyau.fwoopi.old.utils.Utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;


public class ApiInterface {

    private static HashMap<String, String> apiMap = new HashMap<>();

    // API REFERENCE
    //
    static {
        apiMap.put(Constants.API_GET_CATEGORIES, Constants.activeUrl + Constants.API_URL_CATEGORIES + Constants.API_PARAM_KEY
                + Constants.apiKey);
        apiMap.put(Constants.API_GET_USER, Constants.activeUrl + Constants.API_URL_USER_DATA + Constants.API_PARAM_KEY + Constants.apiKey);
        apiMap.put(Constants.API_CREATE_USER, Constants.URL_CREATE_USER);
        apiMap.put(Constants.API_UPDATE_USER, Constants.URL_UPDATE_USER);
        apiMap.put(Constants.API_GET_AD, Constants.activeUrl + Constants.API_URL_USER_AD + Constants.API_PARAM_KEY + Constants.apiKey);
        apiMap.put(Constants.API_GET_USER_STATS, Constants.activeUrl + Constants.API_URL_USER_STATS + Constants.API_PARAM_KEY
                + Constants.apiKey);
        apiMap.put(Constants.API_CANCEL_USER, Constants.activeUrl + Constants.API_URL_USER_CANCEL);
        apiMap.put(Constants.API_REQUEST_PAYMENT, Constants.activeUrl + Constants.API_URL_USER_PAYMENT);
        apiMap.put(Constants.API_VERIFY, Constants.activeUrl + Constants.API_URL_USER_VERIFY);
        apiMap.put(Constants.URL_LOGIN, Constants.URL_LOGIN);
    }


    public static User getUserInfoFromLocale(Context context) {
        SharedPrefsManager sharedPrefsManager = new SharedPrefsManager(context);
        String userJsonString = sharedPrefsManager.retreiveString(Constants.USER_JSON);
        User returnUser = parseUserJsonResponseData(userJsonString);
        UserController.getInstance().setUser(returnUser);
        UserAccess.getInstance(context).setId(returnUser.getId());
        if(!TextUtils.isEmpty(returnUser.getPassword())){
            UserAccess.getInstance(context).setPassword(returnUser.getPassword());
        }
        return returnUser;
    }

    public static User getUserInfoFromNetwork(String id) {
        User returnUser = new User();
        if (id == null || "".equals(id)) {
            return null;
        }
        String url = apiMap.get(Constants.API_GET_USER).replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), id);
        Log.i(ApiInterface.class.getName(), "Request Categories: " + url);
        try {
            URL categoriesUrl = new URL(url);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) categoriesUrl.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.connect();
            int statusCode = httpsURLConnection.getResponseCode();
            if (statusCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();
                String json = sb.toString();

                if ("".equals(json.trim())) {
                    return null;
                }

                JSONObject parsedJson = new JSONObject(json);
                Utils.showDebugLog(ApiInterface.class.getName()+ " :Request Categories: " + parsedJson.toString());
                returnUser.setId(id);
                returnUser.setFirstName(parsedJson.getString(Constants.API_USER_FIRSTNAME));
                returnUser.setLastName(parsedJson.getString(Constants.API_USER_LASTNAME));
                returnUser.setAddress(parsedJson.getString(Constants.API_USER_ADDRESS));
                returnUser.setCity(parsedJson.getString(Constants.API_USER_CITY));
                returnUser.setZip(parsedJson.getString(Constants.API_USER_ZIP));
                returnUser.setState(parsedJson.getString(Constants.API_USER_STATE));
                returnUser.setEmail(parsedJson.getString(Constants.API_USER_EMAIL));
                returnUser.setPhone(parsedJson.getString(Constants.API_USER_PHONE));
                JSONArray cats = parsedJson.getJSONArray(Constants.API_USER_CATEGORIES);
                JSONObject catEntry;
                ArrayList<Category> userCats = new ArrayList<>();
                for (int i = 0; i < cats.length(); i++) {
                    catEntry = cats.getJSONObject(i);
                    Category cat = new Category();
                    cat.setId(catEntry.getString(Constants.API_CATEGORY_ID));
                    cat.setName(catEntry.getString(Constants.API_CATEGORY_NAME));
                    userCats.add(cat);
                }
                returnUser.setCategories(userCats);
                return returnUser;
            }
        } catch (IOException | JSONException ioException) {
            ioException.printStackTrace();
        }

        return null;
    }


    public static User updateUser(User u) {
        User returnUser = new User();
        try {
            String createUserUrl = apiMap.get(Constants.API_UPDATE_USER).replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), UserAccess.getInstance().getId());
            Utils.showDebugLog("updateUser: createUserUrl"+ createUserUrl);
            ArrayList<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair(Constants.API_PARAM_POST_KEY, Constants.apiKey));
            nvps.add(new BasicNameValuePair(Constants.API_USER_PASSWORD,
                    (u.getPassword() == null || "".equals(u.getPassword())) ? UserAccess.getInstance().getPassword() : u.getPassword()));
            if (u.getNewPassword() != null)
                nvps.add(new BasicNameValuePair(Constants.API_USER_NEW_PASSWORD, u.getNewPassword()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_FIRSTNAME, u.getFirstName()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_LASTNAME, u.getLastName()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_ADDRESS, u.getAddress()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_CITY, u.getCity()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_STATE, u.getState()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_ZIP, u.getZip()));
            nvps.add(new BasicNameValuePair(Constants.API_USER_EMAIL, u.getEmail()));
            // nvps.add(new BasicNameValuePair(Constants.API_USER_PHONE,
            // u.getPhone()));

            for (int i = 0; i < u.getCategories().size(); i++)
                nvps.add(new BasicNameValuePair(Constants.API_USER_CATEGORIES_ARRAY, u.getCategories().get(i).getId()));

            Utils.showDebugLog("POST DATA: "+nvps.toString());
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nvps);

            URL url = new URL(createUserUrl);
            HttpsURLConnection request = (HttpsURLConnection) url.openConnection();

            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);

            request.setRequestMethod("POST");
            OutputStream post = request.getOutputStream();
            entity.writeTo(post);
            post.flush();
            Utils.showDebugLog("RESPONSE CODE :::"+request.getResponseCode() + "");
            Utils.showDebugLog("RESPONSE MESSAGE :::"+request.getResponseMessage() + "");
            BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String inputLine, response = "";
            while ((inputLine = in.readLine()) != null) {
                response += inputLine;
            }
            post.close();
            in.close();
            String json = response;

            if ("".equals(json.trim())) {
                return null;
            }

            if (json.contains("400")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("400") + "400".length()));
                ErrorController.getInstance().setHasError(true);
                Utils.showDebugLog("Got an error...");
                return null;
            }

            if (json.contains("401")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("401") + "401".length()));
                ErrorController.getInstance().setHasError(true);
                System.out.println("Got an error...");
                return null;
            }

            JSONObject parsedJson = new JSONObject(json);


            returnUser.setId(parsedJson.getString(Constants.API_USER_ID_FIELD));
            returnUser.setFirstName(parsedJson.getString(Constants.API_USER_FIRSTNAME));
            returnUser.setLastName(parsedJson.getString(Constants.API_USER_LASTNAME));
            returnUser.setAddress(parsedJson.getString(Constants.API_USER_ADDRESS));
            returnUser.setCity(parsedJson.getString(Constants.API_USER_CITY));
            returnUser.setZip(parsedJson.getString(Constants.API_USER_ZIP));
            returnUser.setState(parsedJson.getString(Constants.API_USER_STATE));
            returnUser.setEmail(parsedJson.getString(Constants.API_USER_EMAIL));
            returnUser.setPhone(parsedJson.getString(Constants.API_USER_PHONE));
            // returnUser.setPassword(parsedJson.getString(Constants.API_USER_PASSWORD));
            returnUser.setNewPassword(null);
            JSONArray cats = parsedJson.getJSONArray(Constants.API_USER_CATEGORIES);
            JSONObject catEntry;
            ArrayList<Category> userCats = new ArrayList<>();
            for (int i = 0; i < cats.length(); i++) {
                catEntry = cats.getJSONObject(i);
                Category cat = new Category();
                cat.setId(catEntry.getString(Constants.API_CATEGORY_ID));
                cat.setName(catEntry.getString(Constants.API_CATEGORY_NAME));
                userCats.add(cat);
            }

            returnUser.setCategories(userCats);
            return returnUser;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static User updateUserPassword(User u) {
        User returnUser = new User();
        String updateUserPasswordUrl = apiMap.get(Constants.API_UPDATE_USER).replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), UserAccess.getInstance().getId());
        Utils.showDebugLog("API_UPDATE_USER: "+updateUserPasswordUrl);
        try {
            ArrayList<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair(Constants.API_PARAM_POST_KEY, Constants.apiKey));
            nvps.add(new BasicNameValuePair(Constants.API_USER_PASSWORD, u.getPassword() == null
                    || "".equals(u.getPassword()) ? UserAccess.getInstance().getPassword() : u.getPassword()));
            if (u.getNewPassword() != null)
                nvps.add(new BasicNameValuePair(Constants.API_USER_NEW_PASSWORD, u.getNewPassword()));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nvps);
            URL url = new URL(updateUserPasswordUrl);
            HttpsURLConnection request = (HttpsURLConnection) url.openConnection();
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            request.setRequestMethod("POST");
            OutputStream post = request.getOutputStream();
            entity.writeTo(post);
            post.flush();
            Log.i("RESPONSE CODE :::", request.getResponseCode() + "");
            Log.i("RESPONSE MESSAGE :::", request.getResponseMessage() + "");
            BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String inputLine, response = "";
            while ((inputLine = in.readLine()) != null) {
                response += inputLine;
            }
            post.close();
            in.close();
            String json = response;
            if ("".equals(json.trim())) {
                return null;
            }

            if (json.contains("400")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("400") + "400".length()));
                ErrorController.getInstance().setHasError(true);
                Utils.showDebugLog("Got an error...");
                return null;
            }

            JSONObject parsedJson = new JSONObject(json);
            returnUser.setId(parsedJson.getString(Constants.API_USER_ID_FIELD));
            returnUser.setFirstName(parsedJson.getString(Constants.API_USER_FIRSTNAME));
            returnUser.setLastName(parsedJson.getString(Constants.API_USER_LASTNAME));
            returnUser.setAddress(parsedJson.getString(Constants.API_USER_ADDRESS));
            returnUser.setCity(parsedJson.getString(Constants.API_USER_CITY));
            returnUser.setZip(parsedJson.getString(Constants.API_USER_ZIP));
            returnUser.setState(parsedJson.getString(Constants.API_USER_STATE));
            returnUser.setEmail(parsedJson.getString(Constants.API_USER_EMAIL));
            returnUser.setPhone(parsedJson.getString(Constants.API_USER_PHONE));
            returnUser.setPassword(parsedJson.getString(Constants.API_USER_PASSWORD));
            returnUser.setNewPassword(null);
            JSONArray cats = parsedJson.getJSONArray(Constants.API_USER_CATEGORIES);
            JSONObject catEntry;
            ArrayList<Category> userCats = new ArrayList<>();
            for (int i = 0; i < cats.length(); i++) {
                catEntry = cats.getJSONObject(i);
                Category cat = new Category();
                cat.setId(catEntry.getString(Constants.API_CATEGORY_ID));
                cat.setName(catEntry.getString(Constants.API_CATEGORY_NAME));
                userCats.add(cat);
            }
            returnUser.setCategories(userCats);
            return returnUser;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Ad getNextAd(String id) {
        Ad returnAd = new Ad();
        try {
            URL nextAdURL = new URL(apiMap.get(Constants.API_GET_AD).replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), id));
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) nextAdURL.openConnection();
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setConnectTimeout(5000);
            httpsURLConnection.setReadTimeout(5000);
            httpsURLConnection.connect();
            int statusCode = httpsURLConnection.getResponseCode();
            if (statusCode == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line).append("\n");
                }
                br.close();
                String json = sb.toString();
                if ("".equals(json.trim())) {
                    return null;
                }
                JSONObject parsedJson = new JSONObject(json);
                returnAd.setId(parsedJson.getString(Constants.API_AD_ID));
                returnAd.setText(parsedJson.getString(Constants.API_AD_TEXT));
                returnAd.setLastSent(parsedJson.getLong(Constants.API_AD_LAST));
                return returnAd;
            }
        } catch (IOException | JSONException ioException) {
            ioException.printStackTrace();
            Log.i("Returned Exception: ", ioException.toString());
        }
        return null;
    }

    public static boolean login(String id, String username, String password) {
        String loginUrl = apiMap.get(Constants.URL_LOGIN).replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), id);
        try {
            ArrayList<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair(Constants.API_PARAM_POST_KEY, Constants.apiKey));
            nvps.add(new BasicNameValuePair(Constants.API_USER_PASSWORD, password));
            nvps.add(new BasicNameValuePair(Constants.API_USER_EMAIL, username));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nvps);

            URL url = new URL(loginUrl);
            HttpsURLConnection request = (HttpsURLConnection) url.openConnection();
            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);
            request.setRequestMethod("POST");
            OutputStream post = request.getOutputStream();
            entity.writeTo(post);
            post.flush();
            String s = request.getResponseCode() + "";
            String s2 = request.getResponseMessage() + "";
            BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String inputLine, response = "";
            while ((inputLine = in.readLine()) != null) {
                response += inputLine;
            }
            post.close();
            in.close();
            String json = response;
            Utils.showDebugLog("com.fortyau.fwoopi.api.ApiInterface: "+response);

            if ("".equals(json.trim())) {
                return false;
            }

            if (json.contains("400")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("400") + "400".length()));
                ErrorController.getInstance().setHasError(true);
                Utils.showDebugLog("Got an error...");
                return false;
            }

            if (json.contains("401")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("401") + "401".length()));
                ErrorController.getInstance().setHasError(true);
                System.out.println("Got an error...");
                return false;
            }

            JSONObject parsedJson = new JSONObject(json);
            String amount = parsedJson.getString("Id");
            if (amount != null && !amount.equalsIgnoreCase("")) {
                UserAccess.getInstance().setId(amount);
                UserAccess.getInstance().setPassword(password);
                User u = ApiInterface.parseUserJsonResponseData(json);
                if (u != null)
                    UserController.getInstance().setUser(u);
                return true;
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static User parseUserJsonResponseData(String json) {
        Utils.showDebugLog("Resonse Data :"+json);
        User returnUser = new User();
        try {
            JSONObject parsedJson = new JSONObject(json);
            returnUser.setId(parsedJson.getString(Constants.API_USER_ID_FIELD));
            returnUser.setFirstName(parsedJson.getString(Constants.API_USER_FIRSTNAME));
            returnUser.setLastName(parsedJson.getString(Constants.API_USER_LASTNAME));
            returnUser.setAddress(parsedJson.getString(Constants.API_USER_ADDRESS));
            returnUser.setCity(parsedJson.getString(Constants.API_USER_CITY));
            returnUser.setZip(parsedJson.getString(Constants.API_USER_ZIP));
            returnUser.setState(parsedJson.getString(Constants.API_USER_STATE));
            returnUser.setEmail(parsedJson.getString(Constants.API_USER_EMAIL));
            returnUser.setPhone(parsedJson.getString(Constants.API_USER_PHONE));
            JSONArray cats = parsedJson.getJSONArray(Constants.API_USER_CATEGORIES);
            JSONObject catEntry;
            ArrayList<Category> userCats = new ArrayList<>();
            for (int i = 0; i < cats.length(); i++) {
                catEntry = cats.getJSONObject(i);
                Category cat = new Category();
                cat.setId(catEntry.getString(Constants.API_CATEGORY_ID));
                cat.setName(catEntry.getString(Constants.API_CATEGORY_NAME));
                userCats.add(cat);
            }
            returnUser.setCategories(userCats);
        } catch (JSONException e) {
            e.printStackTrace();
            returnUser = null;
        }
        return returnUser;
    }

}

