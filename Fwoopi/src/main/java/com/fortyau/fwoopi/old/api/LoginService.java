package com.fortyau.fwoopi.old.api;

import android.os.AsyncTask;

import com.fortyau.fwoopi.old.control.ErrorController;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by susan on 1/16/15.
 */
public class LoginService extends AsyncTask<String, Void, Void> {
    private boolean isLoggedIn;
    private RequestPaymentServiceListener listener;

    public LoginService(RequestPaymentServiceListener listener) {
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        listener.requestPaymentTaskStarted();
    }

    @Override
    protected Void doInBackground(String... params) {
        String id = params[0];
        String username = params[1];
        String password = params[2];
        isLoggedIn = login(id, username, password);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        listener.requestPaymentTaskFinished(isLoggedIn);
    }

    private boolean login(String id, String username, String password) {
        String loginUrl = Constants.URL_LOGIN.replaceAll(Pattern.quote(Constants.API_USER_ID_REPLACEMENT), id);
        try {
            ArrayList<NameValuePair> nvps = new ArrayList<>();
            nvps.add(new BasicNameValuePair(Constants.API_PARAM_POST_KEY, Constants.apiKey));
            nvps.add(new BasicNameValuePair(Constants.API_USER_PASSWORD, password));
            nvps.add(new BasicNameValuePair(Constants.API_USER_EMAIL, username));
            UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nvps);

            URL url = new URL(loginUrl);
            HttpsURLConnection request = (HttpsURLConnection) url.openConnection();

            request.setUseCaches(false);
            request.setDoOutput(true);
            request.setDoInput(true);

            request.setRequestMethod("POST");
            OutputStream post = request.getOutputStream();
            entity.writeTo(post);
            post.flush();
            String s = request.getResponseCode() + "";
            String s1 = request.getResponseMessage() + "";
            BufferedReader in = new BufferedReader(new InputStreamReader(request.getInputStream()));
            String inputLine, response = "";
            while ((inputLine = in.readLine()) != null) {
                response += inputLine;
            }
            post.close();
            in.close();
            String json = response;
            Utils.showDebugLog("com.fortyau.fwoopi.api.LoginService: "+json);
            if ("".equals(json.trim())) {
                return false;
            }

            if (json.contains("400")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("400") + "400".length()));
                ErrorController.getInstance().setHasError(true);
                Utils.showDebugLog("Got an error...");
                return false;
            }

            if (json.contains("401")) {
                ErrorController.getInstance().setErrorMessage(json.substring(json.indexOf("401") + "401".length()));
                ErrorController.getInstance().setHasError(true);
                Utils.showDebugLog("Got an error...");
                return false;
            }

            JSONObject parsedJson = new JSONObject(json);
            String amount = parsedJson.getString("Id");
            if (amount != null && !amount.equalsIgnoreCase("")) {
                UserAccess.getInstance().setId(amount);
                UserAccess.getInstance().setPassword(password);
                User user = ApiInterface.parseUserJsonResponseData(json);
                if (user != null)
                    UserController.getInstance().setUser(user);
                return true;

            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();

        }

        return false;
    }
}
