package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.datamodel.Category;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Setup3 extends FwoopiActivity {

    private ProgressDialog pd;
    private List<Category> catArray;
    private StringRequest categoryRequest;
    ArrayList<Category> categoryArrayList;
    RequestQueue queue;
    DefaultRetryPolicy defaultRetryPolicy;
    Spinner catSelect;
    User user;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setup_three);
        user = getIntent().getParcelableExtra("user");
        Button next = (Button) findViewById(R.id.button1);
        next.setText("Finish");
        next.setTextColor(Color.WHITE);
        TextView tv = (TextView) findViewById(R.id.infoAlert);
        tv.setText(Html.fromHtml(getString(R.string.categoryNote)));
        if (!Utils.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
            Toast.makeText(this, "No network connection, please continue once you have internet access", Toast.LENGTH_LONG).show();
            return;
        }

        pd = ProgressDialog.show(Setup3.this, "Loading", "Loading categories from the Fwoopi server...");

        queue = Volley.newRequestQueue(this);
        defaultRetryPolicy = new DefaultRetryPolicy(5000, 1, 1);

        categoryRequest = new StringRequest(Request.Method.GET, Constants.CATEGORIES_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                categoryArrayList = new ArrayList<>();
                try {
                    JSONArray parsedJson = new JSONArray(response);
                    for (int i = 0; i < parsedJson.length(); i++) {
                        JSONObject catEntry = parsedJson.getJSONObject(i);
                        Category cat = new Category();
                        cat.setId(catEntry.getString(Constants.API_CATEGORY_ID));
                        cat.setName(catEntry.getString(Constants.API_CATEGORY_NAME));
                        categoryArrayList.add(cat);
                        catSelect = (Spinner) Setup3.this.findViewById(R.id.Spinner1);
                        ArrayAdapter<Category> cats = new ArrayAdapter<>(Setup3.this, android.R.layout.simple_spinner_item, categoryArrayList);
                        catSelect.setAdapter(cats);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
            }
        });

        queue.add(categoryRequest);


    }

    @Override
    public void handleBackArrow(View view) {
        // TODO Auto-generated method stub

    }

    @Override
    public void handleNextArrow(View view) {
        Spinner catSelect = (Spinner) findViewById(R.id.Spinner1);
        Category cat = (Category) catSelect.getSelectedItem();
        ArrayList<Category> catList = new ArrayList<>();
        catList.add(cat);
        user.setCategories(catList);
        Intent i = new Intent(this, Setup4.class);
        i.putExtra("user",user);
        startActivityForResult(i, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
