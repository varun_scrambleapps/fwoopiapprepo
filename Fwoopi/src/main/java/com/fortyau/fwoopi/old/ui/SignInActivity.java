package com.fortyau.fwoopi.old.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.api.ApiInterface;
import com.fortyau.fwoopi.old.control.UserController;
import com.fortyau.fwoopi.old.datamodel.User;
import com.fortyau.fwoopi.old.datamodel.UserAccess;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.SharedPrefsManager;
import com.fortyau.fwoopi.old.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by macbook on 2/25/16.
 */
public class SignInActivity extends ActionBarActivity {

    private String username, password;
    private EditText usernameEdit, passwordEdit;
    private ProgressDialog progressDialog;
    DefaultRetryPolicy defaultRetryPolicy;
    RequestQueue requestQueue;
    StringRequest loginRequest;
    SharedPrefsManager sharedPrefsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        usernameEdit = (EditText) findViewById(R.id.username);
        passwordEdit = (EditText) findViewById(R.id.password);

        progressDialog = new ProgressDialog(this);
        requestQueue = Volley.newRequestQueue(this);
        defaultRetryPolicy = new DefaultRetryPolicy(5000, 1, 1);
        sharedPrefsManager = new SharedPrefsManager(this);

        loginRequest = new StringRequest(Request.Method.POST, Constants.URL_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Utils.showDebugLog(this.getClass()+" :"+response);
                progressDialog.dismiss();

                JSONObject parsedJson = null;
                try {
                    parsedJson = new JSONObject(response);
                    sharedPrefsManager.saveString(Constants.USER_JSON, response);
                    sharedPrefsManager.saveBoolean(Constants.IS_LOGIN, true);
                    String id = parsedJson.getString(Constants.API_USER_ID_FIELD);
                    if (id != null && !id.equalsIgnoreCase("")) {
                        User user = ApiInterface.parseUserJsonResponseData(response);
                        if (user != null){
                            UserController.getInstance().setUser(user);
                            UserAccess.getInstance(getApplicationContext()).setId(user.getId());
                            if(!TextUtils.isEmpty(user.getPassword())){
                                UserAccess.getInstance(getApplicationContext()).setPassword(user.getPassword());
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Toast.makeText(SignInActivity.this, "Signed In", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(SignInActivity.this, com.fortyau.fwoopi.ui.MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(SignInActivity.this, "Problem:: " + error.toString(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("api_key", Constants.apiKey);
                params.put("Email", username);
                params.put("Password", password);
                return params;
            }
        };

        Button next = (Button) findViewById(R.id.button1);
        next.setText("Login");
        next.setTextColor(Color.WHITE);
    }

    public void handleNextArrow(View view) {
        username = usernameEdit.getText().toString();
        password = passwordEdit.getText().toString();

        if (username.length() <= 0 || !username.contains("@")) {
            Toast.makeText(this, "Please enter an email address", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() <= 0) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }
        loginRequest.setRetryPolicy(defaultRetryPolicy);
        requestQueue.add(loginRequest);
        progressDialog.show();

    }
}
