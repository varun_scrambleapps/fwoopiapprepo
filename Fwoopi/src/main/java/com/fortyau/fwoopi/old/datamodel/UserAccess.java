package com.fortyau.fwoopi.old.datamodel;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.fortyau.fwoopi.old.utils.Constants;

public class UserAccess {
    private Context context;
	private static UserAccess instance;
	private SharedPreferences prefs;
	private Editor editor;
	protected UserAccess() {

	}

    public static UserAccess getInstance() {
		if (instance == null)
			instance = new UserAccess();
		return instance;
	}

	public static UserAccess getInstance(Context context) {
		if (instance == null)
			instance = new UserAccess();
		instance.setContext(context);
		instance.setupSharePrefs();
		return instance;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	private void setupSharePrefs() {
		if (context == null) {
			return;
		}
		prefs = context.getSharedPreferences(Constants.PREFS_FILE, Activity.MODE_PRIVATE);
		editor = prefs.edit();
	}

	public String getPassword() {
		return prefs.getString(Constants.PASSWORD, null);
	}

	public void setPassword(String pass) {
		if (editor == null)
			this.setupSharePrefs();
		editor.putString(Constants.PASSWORD, pass);
		editor.commit();
	}

	public String getId() {
		return prefs.getString(Constants.ID, null);
	}

	public void setId(String id) {
		if (editor == null)
			this.setupSharePrefs();
		
		editor.putString(Constants.ID, id);
		editor.commit();
	}

	public boolean isFirst() {
		return prefs.getBoolean(Constants.FIRST, true);
	}

	public void setFirst(boolean first) {
		editor.putBoolean(Constants.FIRST, first);
		editor.commit();
	}

}
