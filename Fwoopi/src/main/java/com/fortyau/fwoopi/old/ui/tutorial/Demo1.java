package com.fortyau.fwoopi.old.ui.tutorial;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fortyau.fwoopi.R;
import com.fortyau.fwoopi.old.ui.FwoopiActivity;
import com.fortyau.fwoopi.old.ui.Setup4;
import com.fortyau.fwoopi.ui.MainActivity;

public class Demo1 extends FwoopiActivity implements View.OnClickListener {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo_1);

        Button buttonNext = (Button) findViewById(R.id.button1);
        buttonNext.setText("Next");
        buttonNext.setTextColor(Color.WHITE);
        buttonNext.setOnClickListener(this);

        Button buttonSignIn = (Button) findViewById(R.id.Button01);
        buttonSignIn.setText("Sign in");
        buttonSignIn.setTextColor(Color.WHITE);
        buttonSignIn.setOnClickListener(this);


        TextView tv = (TextView) findViewById(R.id.textView2);
        tv.setText(Html.fromHtml(getString(R.string.demoOneNote)));

        tv = (TextView) findViewById(R.id.textView3);
        tv.setText(Html.fromHtml(getString(R.string.howItWorks)));

    }

    public void signIn() {
        Intent i = new Intent(this, com.fortyau.fwoopi.old.v2.ui.SignInActivity.class);
        startActivity(i);
        this.finish();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                Intent i = new Intent(this, Demo2.class);
                startActivityForResult(i, 1);
                break;
            case R.id.Button01:
                signIn();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(Demo1.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    // Nashville 37115

    @Override
    public void handleBackArrow(View view) {

    }

    @Override
    public void handleNextArrow(View view) {

    }
}
