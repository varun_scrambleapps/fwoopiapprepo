package com.fortyau.fwoopi.old.api;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fortyau.fwoopi.FwoopiSMSAppBase;
import com.fortyau.fwoopi.old.Volley.AppController;
import com.fortyau.fwoopi.old.utils.AppTexts;
import com.fortyau.fwoopi.old.utils.Constants;
import com.fortyau.fwoopi.old.utils.Utils;

import org.json.JSONObject;


public class AddService {

    public void requestAdd(final String id, final Response.Listener<JSONObject> listener, final Response.ErrorListener errorListener) {
        Utils.showDebugLog("Add Request Url: " + AppTexts.NEXT_AD_URL + id + AppTexts.API_PARAM_KEY_ADD + AppTexts.apiKey);
        StringRequest addRequest = new StringRequest(Request.Method.GET, AppTexts.NEXT_AD_URL + id + AppTexts.API_PARAM_KEY_ADD + AppTexts.apiKey, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final JSONObject jsonBody = new JSONObject(response);
                    listener.onResponse(jsonBody);
                } catch (Exception e) {

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorListener.onErrorResponse(error);

            }
        }) {
            @Override
            protected VolleyError parseNetworkError(VolleyError volleyError) {
                return volleyError;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
        };
        addRequest.setRetryPolicy(new DefaultRetryPolicy(
                Constants.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        FwoopiSMSAppBase.getInstance().addToRequestQueue(addRequest);
    }
}
