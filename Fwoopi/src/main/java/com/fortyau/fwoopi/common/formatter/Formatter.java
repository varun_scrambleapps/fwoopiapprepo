package com.fortyau.fwoopi.common.formatter;

public interface Formatter {
    String format(String text);
}
