package com.fortyau.fwoopi.interfaces;

import com.fortyau.fwoopi.data.Conversation;

public interface ConversationDetails {
    void showDetails(Conversation c);
}
