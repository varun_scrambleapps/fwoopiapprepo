package com.fortyau.fwoopi.interfaces;

public interface LiveView {
    void refresh(String key);
}
