package com.fortyau.fwoopi.interfaces;

public interface RecipientProvider {
    public String[] getRecipientAddresses();
}
