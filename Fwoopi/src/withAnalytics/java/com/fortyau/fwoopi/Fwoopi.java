package com.fortyau.fwoopi;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class Fwoopi extends FwoopiSMSAppBase {
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
    }
}
